const express = require('express');
const moment = require('moment');
const router = express.Router();

const Post = require('../models/posts');

router.get('/', function(req, res, next) {
    Post.find()
        .exec(function(err, posts) {
            if (err) {
                return res.status(500).json({
                    title: 'An error occurred',
                    error: err
                });
            }
            res.status(200).json({
                message: 'Success',
                obj: posts
            });
        });
});

router.get('/:id', function(req, res, next) {
    Post.findById(req.params.id)
        .exec(function (err, post) {
            if (err) {
                return res.status(500).json({
                    title: 'An error occurred',
                    error: err
                });
            }
            res.status(200).json({
                message: 'Success',
                obj: post
            });
        });
});

router.post('/new', function(req, res, next) {
    let post = new Post({
        title: req.body.title,
        content: req.body.content,
        createAt: moment().format('MMMM Do YYYY, h:mm:ss a')
    });
    post.save(function (err, result) {
        if (err) {
            return res.status(500).json({
                title: 'An error occurred',
                error: err
            });
        }
        res.status(201).json({
            message: 'Post created',
            obj: result
        });
    });
});

router.patch('/:id', function(req, res, next) {
    Post.findById(req.params.id)
        .exec(function(err, post) {
            if (err) {
                return res.status(500).json({
                    title: 'An error occurred',
                    error: err
                });
            }
            if (!post) {
                return res.status(500).json({
                    title: 'No Post Found!',
                    error: {post: 'Post not found'}
                });
            }
            post.title = req.body.title;
            post.content = req.body.content;
            post.save(function(err, result) {
                if (err) {
                    return res.status(500).json({
                        title: 'An error occurred',
                        error: err
                    });
                }
                res.status(200).json({
                    message: 'Updated message',
                    obj: result
                });
            });
        });
});

router.delete('/:id', function(req, res, next) {
    Post.findByIdAndRemove(req.params.id)
        .exec(function (err, post) {
            if (err) {
                return res.status(500).json({
                    title: 'An error occurred',
                    error: err
                });
            }
            if (!post) {
                return res.status(500).json({
                    title: 'No Post Found!',
                    error: {post: 'Post not found'}
                });
            }
            else {
                res.status(200).json({
                    message: 'Deleted message',
                    obj: post
                });
            }
        });
});

module.exports = router;
