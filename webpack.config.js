const path = require('path');
const htmlWebpackPlugin = require('html-webpack-plugin');
const cleanWebpackPlugin = require('clean-webpack-plugin');
const liveReloadPlugin = require('webpack-livereload-plugin');

module.exports = {
    entry: './public/index.js',
    plugins: [
        new htmlWebpackPlugin({
            title: 'Blog - React+Redux',
            template: './public/index.html'
        }),
        new cleanWebpackPlugin(['build']),
        new liveReloadPlugin()
    ],
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'build')
    },
    devServer: {
        contentBase: './build'
    },
    devtool: 'cheap-module-source-map',
    module: {
        loaders: [
            {
                test: /\.css$/,
                loader: ['style-loader','css-loader']
            },
            {
                test: /\.(png|jpe?g|svg|gif)$/,
                loader: 'file-loader'
            },
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
                query: {
                    presets: ['react', 'es2015']
                }
            },
            {
                test: /\.json$/,
                loader: 'json-loader'
            }
        ]
    },
    resolve: {
        extensions: ['.js', '.jsx']
    }
};