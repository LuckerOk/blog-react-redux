import postsActions from './postsActions';
import usersActions from './usersActions';

const actions = [
  ...postsActions, ...usersActions
];

export default actions;
