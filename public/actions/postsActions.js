import { createLogic } from 'redux-logic';
import axios from 'axios';

//Post list
export const FETCH_POSTS = 'FETCH_POSTS';
export const FETCH_POSTS_SUCCESS = 'FETCH_POSTS_SUCCESS';
export const FETCH_POSTS_FAILURE = 'FETCH_POSTS_FAILURE';

//Create new post
export const CREATE_POST = 'CREATE_POST';
export const CREATE_POST_SUCCESS = 'CREATE_POST_SUCCESS';
export const CREATE_POST_FAILURE = 'CREATE_POST_FAILURE';

//Delete post
export const DELETE_POST = 'DELETE_POST';
export const DELETE_POST_SUCCESS = 'DELETE_POST_SUCCESS';
export const DELETE_POST_FAILURE = 'DELETE_POST_FAILURE';

export function fetchPosts(request) {
  return {
    type: FETCH_POSTS,
    payload: request
  };
}

export function fetchPostsSuccess(posts) {
  return {
    type: FETCH_POSTS_SUCCESS,
    payload: posts
  };
}

export function fetchPostsFailure(error) {
  return {
    type: FETCH_POSTS_FAILURE,
    payload: error
  };
}

export function createPost(request) {
  return {
    type: CREATE_POST,
    payload: request
  };
}

export function createPostSuccess(newPost) {
  return {
    type: CREATE_POST_SUCCESS,
    payload: newPost
  };
}

export function createPostFailure(error) {
  return {
    type: CREATE_POST_FAILURE,
    payload: error
  };
}

export function deletePost(request) {
  return {
    type: DELETE_POST,
    payload: request
  };
}

export function deletePostSuccess(deletedPost) {
  return {
    type: DELETE_POST_SUCCESS,
    payload: deletedPost
  };
}

export function deletePostFailure(response) {
  return {
    type: DELETE_POST_FAILURE,
    payload: response
  };
}

let instance = axios.create({
  headers: {'Content-Type': 'application/json'}
});

const createPostLogic = createLogic({
  type: CREATE_POST,
  latest: true,

  process({getState, action}, dispatch, done) {
    return instance.post('http://localhost:3000/posts/new', {
      title: action.payload.title,
      content: action.payload.content
    }).then(res => { dispatch({ type: CREATE_POST_SUCCESS,
      payload: res }); done(); })
      .catch(res => { dispatch({ type: CREATE_POST_FAILURE,
        payload: res }); done(); });
  }
});

const getPostLogic = createLogic({
  type: FETCH_POSTS,
  latest: true,

  process({getState, action}, dispatch, done) {
    return instance.get('http://localhost:3000/posts')
      .then(res => { dispatch({ type: FETCH_POSTS_SUCCESS,
        payload: res.data }); done(); })
      .catch(res => { dispatch({ type: FETCH_POSTS_FAILURE,
        payload: res }); done(); });
  }
});

export default [ createPostLogic, getPostLogic ];