import { createLogic } from 'redux-logic';
import axios from 'axios';

//Sign Up User
export const SIGNUP_USER = 'SIGNUP_USER';
export const SIGNUP_USER_SUCCESS = 'SIGNUP_USER_SUCCESS';
export const SIGNUP_USER_FAILURE = 'SIGNUP_USER_FAILURE';

//Sign In User
export const SIGNIN_USER = 'SIGNIN_USER';
export const SIGNIN_USER_SUCCESS = 'SIGNIN_USER_SUCCESS';
export const SIGNIN_USER_FAILURE = 'SIGNIN_USER_FAILURE';

//log out user
export const LOGOUT_USER = 'LOGOUT_USER';

export function signUpUser(formValues) {

  return {
    type: SIGNUP_USER,
    payload: formValues
  };
}

export function signUpUserSuccess(user) {
  return {
    type: SIGNUP_USER_SUCCESS,
    payload: user
  };
}

export function signUpUserFailure(error) {
  return {
    type: SIGNUP_USER_FAILURE,
    payload: error
  };
}

export function signInUser(formValues) {

  return {
    type: SIGNIN_USER,
    payload: formValues
  };
}

export function signInUserSuccess(user) {
  return {
    type: SIGNIN_USER_SUCCESS,
    payload: user
  };
}

export function signInUserFailure(error) {
  return {
    type: SIGNIN_USER_FAILURE,
    payload: error
  };
}

export function logoutUser() {
  return {
    type: LOGOUT_USER
  };
}

let instance = axios.create({
  headers: {'Content-Type': 'application/json'}
});

const signupUserLogic = createLogic({
  type: SIGNUP_USER,
  latest: true,

  process({getState, action}, dispatch, done) {
    return instance.post('http://localhost:3000/users/signup', {
      username: action.payload.username,
      password: action.payload.password,
      firstName: action.payload.firstName,
    }).then(res => { dispatch({ type: SIGNUP_USER_SUCCESS,
      payload: res }); done(); })
      .catch(res => { dispatch({ type: SIGNUP_USER_FAILURE,
        payload: res }); done(); });
  }
});

const signinUserLogic = createLogic({
  type: SIGNIN_USER,
  latest: true,

  process({getState, action}, dispatch, done) {
    return instance.post('http://localhost:3000/users/signin', {
      username: action.payload.username,
      password: action.payload.password
    }).then(res => { dispatch({ type: SIGNIN_USER_SUCCESS,
      payload: res }); done(); })
      .catch(res => { dispatch({ type: SIGNIN_USER_FAILURE,
        payload: res }); done(); });
  }
});

export default [ signupUserLogic, signinUserLogic ];