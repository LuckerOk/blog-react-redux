import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { HashRouter as Router } from 'react-router-dom';
import Routes from './Routes';
import configureStore from './store/configureStore';
import './style/style.css';

ReactDOM.render(
  <MuiThemeProvider>
    <Provider store={configureStore}>
      <Router>
        <Routes />
      </Router>
    </Provider>
  </MuiThemeProvider>,
  document.getElementById('body')
);