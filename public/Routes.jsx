import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';

import App from './pages/App';
import PostsIndex from './pages/PostsIndex';
import PostsNew from './pages/PostsNew';
import SignIn from './pages/SignIn';
import SignUp from './pages/SignUp';
import Profile from './pages/Profile';

class Routes extends Component {
    render() {
        return (
            <Switch>
                <Route exact path="/" component={App} />
                <Route path="/posts/new" component={PostsNew} />
                <Route path="/posts" component={PostsIndex} />
                <Route path="/signin" component={SignIn} />
                <Route path="/signup" component={SignUp} />
                <Route path="/profile" component={Profile} />
            </Switch>
        )
    }
}

export default Routes;