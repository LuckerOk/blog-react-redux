import React, { Component } from 'react';
import {Card, CardHeader, CardText} from 'material-ui/Card';

class PostsList extends Component {
    constructor(props) {
        super(props);
    }

    componentWillMount() {
        this.props.fetchPosts();
    }

    renderPosts(posts) {
        return posts.map((post) => {
            return (
                <Card className="card" key={post._id}>
                    <CardHeader
                        title={post.title}
                        subtitle={post.createAt}
                    />
                    <CardText>
                        {post.content}
                    </CardText>
                </Card>
            );
        });
    }

    render() {
        const posts= this.props.postsList.posts;

        return (
            <div className="container">
                {this.renderPosts(posts)}
            </div>
        );
    }
}


export default PostsList;
