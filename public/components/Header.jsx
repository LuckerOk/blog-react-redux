import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import AppBar from 'material-ui/AppBar';
import {ToolbarGroup} from 'material-ui/Toolbar';
import FlatButton from 'material-ui/FlatButton';

class Header extends Component {

    renderLinks() {
            return (
                <AppBar
                    iconElementRight={
                        <ToolbarGroup>
                            <FlatButton label="Profile" containerElement={<Link to="/profile"/>}/>
                            <FlatButton label="Log out" containerElement={<Link to="/"/>}/>
                        </ToolbarGroup>
                    }
                    iconElementLeft={
                        <ToolbarGroup>
                            <FlatButton label="Posts" containerElement={<Link to="/posts"/>}/>
                            <FlatButton label="New Post" containerElement={<Link to="/posts/new"/>}/>
                        </ToolbarGroup>
                    }
                />
            );
    }

    render() {
        return (
            <div>
                {this.renderLinks()}
            </div>
        );
    }
}

export default Header