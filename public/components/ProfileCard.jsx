import React from 'react';
import { Component } from 'react';
import {Card, CardHeader, CardText} from 'material-ui/Card';

export default class ProfileCard extends Component {

    render() {
        let user = this.props.user.user;
        return (
            <div className="container">
                <Card className="card" >
                    <CardHeader
                        title="Profile"
                    />
                    <CardText>
                        {user.username}
                        {user.firstName}
                    </CardText>
                </Card>
            </div>
        );
    }
}
