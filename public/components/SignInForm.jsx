import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';

class SignInForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            username: '',
            password: ''
        };

        this.handleUsernameChange = this.handleUsernameChange.bind(this);
        this.handlePasswordChange = this.handlePasswordChange.bind(this);
        this.submitUserData = this.submitUserData.bind(this);
    }

    handleUsernameChange(e) {
        console.log("Username: " + e.target.value);
        this.setState({
            username: e.target.value
        });
    }

    handlePasswordChange(e) {
        console.log("Password: " + e.target.value);
        this.setState({
            password: e.target.value
        });
    }

    submitUserData() {
        this.props.onSubmit(this.state);
    }

    render() {
        return (
            <div className="container">
                <Paper className="paper" zDepth={2} >
                    <h1>Sign in</h1>
                    <TextField
                        hintText="Username"
                        floatingLabelText="Username"
                        floatingLabelFixed={true}
                        fullWidth={true}
                        value={this.state.username}
                        onChange={this.handleUsernameChange}
                    />
                    <TextField
                        hintText="Password"
                        floatingLabelText="Password"
                        floatingLabelFixed={true}
                        fullWidth={true}
                        type="password"
                        value={this.state.password}
                        onChange={this.handlePasswordChange}
                    />
                    <RaisedButton
                        className="raisedButton-submit"
                        label="SIGN IN"
                        secondary={true}
                        fullWidth={true}
                        onClick={this.submitUserData}
                    />
                    <div className="auth-bottom-text">
                        Don't have an account?
                        <Link className="auth-sign-link" to="/signup">
                            Sign up
                        </Link>
                    </div>
                </Paper>
            </div>
        );
    }
}

export default SignInForm;