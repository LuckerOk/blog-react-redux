import React from 'react';
import { Component } from 'react';
import Paper from 'material-ui/Paper';
import RaisedButton from 'material-ui/RaisedButton';
import { Link } from 'react-router-dom';

export default class App extends Component {
    render() {
        return (
            <div className="container">
                <Paper className="paper" zDepth={2} >
                    <h2>Welcome to React Blog App</h2>
                    <RaisedButton
                        className="raisedButton-submit"
                        label="SIGN IN"
                        secondary={true}
                        fullWidth={true}
                        containerElement={<Link to="/signin" />}
                    />
                    <RaisedButton
                        className="raisedButton-submit"
                        label="SIGN UP"
                        secondary={true}
                        fullWidth={true}
                        containerElement={<Link to="/signup" />}
                    />
                </Paper>
            </div>
        );
    }
}
