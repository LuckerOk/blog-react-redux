import React, { Component } from 'react';
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';

class PostsForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            title: '',
            content: ''
        };

        this.handleTitleChange = this.handleTitleChange.bind(this);
        this.handleContentChange = this.handleContentChange.bind(this);
        this.submitPostData = this.submitPostData.bind(this);
    }

    handleTitleChange(e) {
        console.log("Title: " + e.target.value);
        this.setState({
            title: e.target.value
        });
    }

    handleContentChange(e) {
        console.log("Content: " + e.target.value);
        this.setState({
            content: e.target.value
        });
    }

    submitPostData() {
        this.props.onSubmit(this.state);
    }



    render() {
        return (
            <div className="container">
                <Paper className="paper" zDepth={2} >
                    <h1>Post</h1>
                    <TextField
                        hintText="Title"
                        floatingLabelText="Title"
                        floatingLabelFixed={true}
                        fullWidth={true}
                        value={this.state.title}
                        onChange={this.handleTitleChange}
                    />
                    <TextField
                        hintText="Content"
                        floatingLabelText="Content"
                        floatingLabelFixed={true}
                        multiLine={true}
                        rows={3}
                        rowsMax={7}
                        fullWidth={true}
                        value={this.state.content}
                        onChange={this.handleContentChange}
                    />
                    <RaisedButton
                        className="raisedButton-submit"
                        label="Post"
                        secondary={true}
                        fullWidth={true}
                        onClick={this.submitPostData}
                    />
                </Paper>
            </div>
        );
    }
}

export default PostsForm;