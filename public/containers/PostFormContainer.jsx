import PostsForm from '../components/PostsForm';
import { connect } from 'react-redux';
import { createPost } from "../actions/postsActions";

const mapDispatchToProps = (dispatch) => {
    return {
        onSubmit: (payload) => {
            dispatch(createPost(payload))
        }
    }
};

export default connect(null, mapDispatchToProps)(PostsForm);