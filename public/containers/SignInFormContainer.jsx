import SignInForm from '../components/SignInForm';
import { connect } from 'react-redux';
import { signInUser } from "../actions/usersActions";

const mapDispatchToProps = (dispatch) => {
    return {
        onSubmit: (payload) => {
            dispatch(signInUser(payload))
        }
    }
};

export default connect(null, mapDispatchToProps)(SignInForm);