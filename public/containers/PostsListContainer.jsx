import { connect } from 'react-redux'
import { fetchPosts } from '../actions/postsActions';
import PostsList from '../components/PostsList';

const mapStateToProps = (state) => {
    console.log(state);
    return {
        postsList: state.posts.postsList
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchPosts: () => {
            dispatch(fetchPosts())
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(PostsList);