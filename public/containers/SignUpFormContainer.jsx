import SignUpForm from '../components/SignUpForm';
import { connect } from 'react-redux';
import {signUpUser} from "../actions/usersActions";

const mapDispatchToProps = (dispatch) => {
    return {
        onSubmit: (payload) => {
            dispatch(signUpUser(payload))
        }
    }
};

export default connect(null, mapDispatchToProps)(SignUpForm);