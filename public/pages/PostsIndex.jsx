import React, { Component } from 'react';
import HeaderContainer from '../containers/HeaderContainer.jsx';
import PostsList from '../containers/PostsListContainer.jsx';

class PostsIndex extends Component {
    render() {
        return (
            <div>
                <HeaderContainer />
                <PostsList />
            </div>
        );
    }
}


export default PostsIndex;
