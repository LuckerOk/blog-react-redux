import React, { Component } from 'react';
import HeaderContainer from '../containers/HeaderContainer';
import ProfileCardContainer from '../containers/ProfileCardContainer';

class Profile extends Component {
    render() {
        return (
            <div>
                <HeaderContainer />
                <ProfileCardContainer />
            </div>
        );
    }
}


export default Profile;
