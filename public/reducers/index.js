import { combineReducers } from 'redux';
import UserReducer from './usersReducer';
import PostsReducer from './postReducer';

const rootReducer = combineReducers({
  user: UserReducer,
  posts: PostsReducer
});

export default rootReducer;