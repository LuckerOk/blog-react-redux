import {
  SIGNUP_USER, SIGNUP_USER_SUCCESS, SIGNUP_USER_FAILURE,
  SIGNIN_USER, SIGNIN_USER_SUCCESS,  SIGNIN_USER_FAILURE,
  LOGOUT_USER
} from '../actions/usersActions';

const INITIAL_STATE = {user: null, status:null, error:null, loading: false};

export default function(state = INITIAL_STATE, action) {
  switch(action.type) {

    case SIGNUP_USER:// sign up user, set loading = true and status = signup
      return { ...state, user: null, status:'signup', error:null, loading: true};
    case SIGNUP_USER_SUCCESS://return user, status = authenticated and make loading = false
      return { ...state, user: action.payload.user, status:'authenticated', error:null, loading: false}; //<-- authenticated
    case SIGNUP_USER_FAILURE:// return error and make loading = false
      return { ...state, user: null, status:'signup', error:'true', loading: false};


    case SIGNIN_USER:// sign in user,  set loading = true and status = signin
      return { ...state, user: null, status:'signin', error:null, loading: true};
    case SIGNIN_USER_SUCCESS://return authenticated user,  make loading = false and status = authenticated
      return { ...state, user: action.payload.user, status:'authenticated', error:null, loading: false}; //<-- authenticated
    case SIGNIN_USER_FAILURE:// return error and make loading = false
      return { ...state, user: null, status:'signin', error:'true', loading: false};


    case LOGOUT_USER:
      return {...state, user:null, status:'logout', error:null, loading: false};

    default:
      return state;
  }
}