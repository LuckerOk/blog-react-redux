import { applyMiddleware, createStore } from 'redux';
import { createLogicMiddleware } from 'redux-logic';
import logger from 'redux-logger';
import reducer from '../reducers';
import actions from '../actions/index';

let logicMiddleware = createLogicMiddleware(actions);
let configureStore = createStore(reducer, applyMiddleware(logicMiddleware, logger));

export default configureStore;