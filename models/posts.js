const mongoose = require('mongoose');
const Schema = mongoose.Schema;
mongoose.Promise = global.Promise;

const postSchema = new Schema({
    title: { type: String },
    content: { type: String, required: true },
    createAt: { type: String },
    authorName: mongoose.Schema.ObjectId
});

//postSchema.belongsTo('User', {through: 'author'});

module.exports = mongoose.model('Post', postSchema);