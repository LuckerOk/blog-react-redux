const mongoose = require('mongoose');
const Schema = mongoose.Schema;
mongoose.Promise = global.Promise;

const userSchema = new Schema({
    username: { type: String, required: true },
    password: { type: String, required: true },
    firstName: { type: String, required: true },
    subscriptions: mongoose.Schema.Types.ObjectId,
    followers: mongoose.Schema.Types.ObjectId,
    posts: [mongoose.Schema.ObjectId],
    rights: { type: String, default: 'USER' }
});

//userSchema.hasMany('Post', {dependent: 'delete'});

module.exports = mongoose.model('User', userSchema);